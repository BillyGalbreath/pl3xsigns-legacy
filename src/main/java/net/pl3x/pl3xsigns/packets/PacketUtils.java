package net.pl3x.pl3xsigns.packets;

import java.lang.reflect.InvocationTargetException;

import net.pl3x.pl3xsigns.Pl3xSigns;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.events.PacketEvent;

public class PacketUtils {
	private static ProtocolManager packetManager = ProtocolLibrary.getProtocolManager();
	
	/*
	 * Send packet to open sign editor window
	 */
	public static void sendSignWindowPacket(Player player, Sign sign) throws InvocationTargetException {
		PacketContainer packet = packetManager.createPacket(PacketType.Play.Server.OPEN_SIGN_ENTITY);
		packet.getIntegers()
			.write(0, sign.getX())
			.write(1, sign.getY())
			.write(2, sign.getZ());
		packetManager.sendServerPacket(player, packet);
	}
	
	/*
	 * Register the packet listener (only do this once!)
	 */
	public static void registerPacketReceiver(Pl3xSigns plugin) {
		packetManager.addPacketListener(new PacketAdapter(plugin, PacketType.Play.Client.UPDATE_SIGN) {
			@Override
			public void onPacketReceiving(PacketEvent event) {
				PacketContainer packet = event.getPacket();
				String[] lines = packet.getStringArrays().read(0);
				Integer x = packet.getIntegers().read(0);
				Integer y = packet.getIntegers().read(1);
				Integer z = packet.getIntegers().read(2);
				if (x == null || y == null || z == null)
					return;
				Sign sign = (Sign) (new Location(event.getPlayer().getWorld(), x, y, z)).getBlock().getState();
				if (!Pl3xSigns.containsSign(sign))
					return;
				Pl3xSigns.removeSign(sign);
				for (int i = 0; i < 4; i++) {
					sign.setLine(i, lines[i]);
				}
				((Pl3xSigns)Bukkit.getPluginManager().getPlugin("Pl3xSigns")).updateSign(event.getPlayer(), sign);
			}
		});
	}
}
