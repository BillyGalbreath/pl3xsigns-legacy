package net.pl3x.pl3xsigns.commands;

import net.pl3x.pl3xsigns.Pl3xSigns;

import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CmdSignEdit implements CommandExecutor {
	private Pl3xSigns plugin;
	
	public CmdSignEdit(Pl3xSigns plugin) {
		this.plugin = plugin;
	}
	
	private String getFinalArg(String[] args, int start) {
		StringBuilder bldr = new StringBuilder();
		for (int i = start; i < args.length; i++) {
			if (i != start)
				bldr.append(" ");
			bldr.append(args[i]);
		}
		return bldr.toString();
	}
	
	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (!cmd.getName().equalsIgnoreCase("signedit"))
			return false;
		if (!cs.hasPermission("pl3xsigns.command.signedit")) {
			cs.sendMessage(plugin.colorize("&4You don't have permission for that!"));
			plugin.log(cs.getName() + " was denied access to that command!");
			return true;
		}
		if (!(cs instanceof Player)) {
			cs.sendMessage(plugin.colorize("&4This command is only available to players!"));
			return true;
		}
		Player player = (Player) cs;
		if (args.length < 1) {
			player.sendMessage(plugin.colorize("&4You must specify a line number to edit!"));
			return true;
		}
		Block block = player.getTargetBlock(null, 10);
		if (block == null || !(block.getState() instanceof Sign)) {
			player.sendMessage(plugin.colorize("&4Not looking at a sign!"));
			return true;
		}
		if (!plugin.canAdminProtection((Player) cs, block)) {
			player.sendMessage(plugin.colorize("&4You do not own this sign!"));
			return true;
		}
		Integer line;
		try {
			line = Integer.parseInt(args[0]);
			line--;
		} catch(NumberFormatException e) {
			player.sendMessage(plugin.colorize("&4Line must be a number!"));
			return true;
		}
		if ((line < 0) || (line > 3)) {
			player.sendMessage(plugin.colorize("&4Acceptable line range is 1 - 4 only!"));
			return true;
		}
		Sign sign = (Sign) block.getState();
		String newline = (args.length < 2) ? "" : getFinalArg(args,1);
		sign.setLine(line, newline);
		plugin.updateSign(player, sign);
		return true;
	}
}
