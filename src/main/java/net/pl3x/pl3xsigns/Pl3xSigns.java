package net.pl3x.pl3xsigns;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import net.pl3x.pl3xsigns.commands.CmdSignEdit;
import net.pl3x.pl3xsigns.listeners.SignListener;
import net.pl3x.pl3xsigns.packets.PacketUtils;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.Logger;
import org.apache.logging.log4j.message.Message;
import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.mcstats.Metrics;

import com.griefcraft.lwc.LWC;
import com.griefcraft.lwc.LWCPlugin;

public class Pl3xSigns extends JavaPlugin {
	private PluginManager pm = Bukkit.getPluginManager();
	private LWC lwc;
	private boolean useProtocolLib = false;
	private static List<Sign> signs = new ArrayList<Sign>();
	
	public void onEnable() {
		if (!new File(getDataFolder() + File.separator + "config.yml").exists())
			saveDefaultConfig();
		
		if (pm.isPluginEnabled("ProtocolLib")) {
			log("ProtocolLib found! Enabling packet handlers!");
			useProtocolLib = true;
			PacketUtils.registerPacketReceiver(this);
		}
		if (pm.isPluginEnabled("LWC")) {
			log("LWC found! Enabling sign protections!");
			lwc = ((LWCPlugin) pm.getPlugin("LWC")).getLWC();
		}
		
		pm.registerEvents(new SignListener(this), this);
		
		getCommand("signedit").setExecutor(new CmdSignEdit(this));
		
		reloadFilter();
		
		try {
			Metrics metrics = new Metrics(this);
			metrics.start();
		} catch (IOException e) {
			log("&4Failed to start Metrics: &e" + e.getMessage());
		}
		
		log(getName() + " v" + getDescription().getVersion() + " by BillyGalbreath enabled!");
	}
	
	public void onDisable() {
		log(getName() + " Disabled.");
	}
	
	public void log (Object obj) {
		if (getConfig().getBoolean("color-logs", true))
			getServer().getConsoleSender().sendMessage(colorize("&3[&d" +  getName() + "&3]&r " + obj));
		else
			Bukkit.getLogger().log(java.util.logging.Level.INFO, "[" + getName() + "] " + ((String) obj).replaceAll("(?)\u00a7([a-f0-9k-or])", ""));
	}
	
	public String colorize(String str) {
		return str.replaceAll("(?i)&([a-f0-9k-or])", "\u00a7$1");
	}
	
	public boolean useProtocolLib() {
		return useProtocolLib;
	}
	
	public boolean canAdminProtection(Player p, Block b) {
		if (lwc == null)
			return true;
		return (hasProtection(b)) ? lwc.canAdminProtection(p, b) : true;
	}
	
	public void updateSign(Player player, final Sign sign) {
		for (int i = 0; i < 4; i++) {
			if (player.hasPermission("pl3xsigns.signs.color"))
				sign.setLine(i, colorize(sign.getLine(i)));
		}
		player.sendMessage(colorize("&dSign updated."));
		sign.update();
	}
	
	public static boolean containsSign(Sign sign) {
		return signs.contains(sign);
	}
	
	public static void addSign(Sign sign) {
		if (signs.contains(sign))
			return;
		signs.add(sign);
	}
	
	public static void removeSign(Sign sign) {
		if (!signs.contains(sign))
			return;
		signs.remove(sign);
	}
	
	private boolean hasProtection(Block b) {
		if (lwc == null)
			return false;
		return (lwc.findProtection(b) == null) ? false : true;
	}
	
	public void reloadFilter() {
		org.apache.logging.log4j.core.Logger coreLogger = (org.apache.logging.log4j.core.Logger) LogManager.getRootLogger();
		Filter filter = new Filter() {
			@Override
			public Result filter(LogEvent event) {
				if (event.getLevel().equals(Level.WARN) && event.getMessage().toString().contains("just tried to change non-editable sign"))
					return Result.DENY;
				return null;
			}
			@Override
			public Result filter(Logger arg0, Level arg1, Marker arg2, String arg3, Object... arg4) {
				return null;
			}
			@Override
			public Result filter(Logger arg0, Level arg1, Marker arg2, Object arg3, Throwable arg4) {
				return null;
			}
			@Override
			public Result filter(Logger arg0, Level arg1, Marker arg2, Message arg3, Throwable arg4) {
				return null;
			}
			@Override
			public Result getOnMatch() {
				return null;
			}
			@Override
			public Result getOnMismatch() {
				return null;
			}
		};
		boolean alreadyLoaded = false;
		Iterator<Filter> iter = coreLogger.getFilters();
		while (iter.hasNext()) {
			if (filter.equals(iter.next())) {
				alreadyLoaded = true;
				break;
			}
		}
		if (!alreadyLoaded)
			coreLogger.addFilter(filter);
	}
}
