package net.pl3x.pl3xsigns.listeners;

import java.lang.reflect.InvocationTargetException;

import net.pl3x.pl3xsigns.Pl3xSigns;
import net.pl3x.pl3xsigns.packets.PacketUtils;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class SignListener implements Listener {
	private Pl3xSigns plugin;
	
	public SignListener(Pl3xSigns plugin) {
		this.plugin = plugin;
	}
	
	/*
	 * Prevent signs from going blank (fixes LWC bug)
	 */
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onSignWentBlank(PlayerInteractEvent event) {
		if(!event.hasBlock())
			return;
		if (event.getAction() != Action.LEFT_CLICK_BLOCK)
			return;
		Block block = event.getClickedBlock();
		if (block == null || !(block.getState() instanceof Sign))
			return;
		((Sign) block.getState()).update();
		if (plugin.getConfig().getBoolean("debug-mode", false))
			plugin.log("Sign text refreshed. (lwc bug fix)");
	}
	
	/*
	 * Handle styling of signs (LWC protections are checked too)
	 */
	@EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
	public void onStyleSign(SignChangeEvent event) {
		Player player = event.getPlayer();
		if (!plugin.canAdminProtection(player, event.getBlock())) {
			player.sendMessage(plugin.colorize("&4You do not own this sign!"));
			return;
		}
		Sign sign = (Sign) event.getBlock().getState();
		for (int i = 0; i < 4; i++) {
			sign.setLine(i, event.getLine(i));
		}
		plugin.updateSign(event.getPlayer(), sign);
		if (plugin.getConfig().getBoolean("debug-mode", false))
			plugin.log("Sign change detected!");
	}
	
	/*
	 * Open sign editor window if sign was placed on another sign
	 */
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void signPlace(BlockPlaceEvent event) {
		if (!plugin.useProtocolLib())
			return;
		Block block = event.getBlockPlaced();
		if (block.getType() != Material.WALL_SIGN && block.getType() != Material.SIGN_POST)
			return;
		Block blockAgainst = event.getBlockAgainst();
		if (!(blockAgainst.getState() instanceof Sign))
			return;
		Player player = event.getPlayer();
		if (!player.hasPermission("pl3xsigns.click.signedit"))
			return;
		if (player.isSneaking())
			return;
		if (!plugin.canAdminProtection(player, block))
			return;
		Sign sign = (Sign) blockAgainst.getState();
		if (Pl3xSigns.containsSign(sign)) {
			player.sendMessage(plugin.colorize("&4Someone is already editing this sign!"));
			event.setCancelled(true);
			return;
		}
		if (plugin.getConfig().getBoolean("debug-mode", false))
			plugin.log(player.getName() + " creating sign editor.");
		try {
			PacketUtils.sendSignWindowPacket(player, sign);
		} catch (InvocationTargetException e) {
			if (plugin.getConfig().getBoolean("debug-mode", false))
				plugin.log("&4Error sending sign editor packet!");
			return;
		}
		Pl3xSigns.addSign(sign);
		event.setCancelled(true);
	}
}
